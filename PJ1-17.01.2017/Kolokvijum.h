#include "IPrintable.h"
#include <unordered_set>
#include <functional>
#include <initializer_list>
#include <stdexcept>
#include <type_traits>

template<class T>
class FilterException;

template<class T, long long N = 0>
class FilteredSet : protected std::unordered_set<T>, public IPrintable
{
private:
	const std::function<bool(const T&)>& f;
public:
	FilteredSet(const std::function<bool(const T&)>& ff) noexcept(false) : f(ff) {}
	void print(std::ostream&) const noexcept override;
	int size() const noexcept;
	virtual void add(const std::initializer_list<T>& list) noexcept(false);

	FilteredSet& operator= (const FilteredSet&) = delete;
	FilteredSet& operator= (FilteredSet&&) = delete;
};

template<class T, long long N>
void FilteredSet<T, N>::print(std::ostream& os) const noexcept
{
	for (auto it = begin(); it != end(); ++it)
		os << *it << ' ';
}

template<class T, long long N>
int FilteredSet<T, N>::size() const noexcept
{
	return std::unordered_set<T>::size();
}

template<class T, long long N>
void FilteredSet<T, N>::add(const std::initializer_list<T>& list) noexcept(false)
{
	static_assert(std::is_copy_constructible<T>::value, "Not copy constructable");
	static_assert(N >= 0, "Invalid input");
	FilterException<int> tempExcept;
	for (auto a : list)
		if (f(a) && (N == 0 || size() < N))
			emplace(a);
		else
			tempExcept.add({ a });
	if (tempExcept.size())
		throw tempExcept;
}

template<class T>
class FilterException : public FilteredSet<T>, public std::exception
{
private:
	const std::function<bool(const T&)> f = [](const T& a) { return true; };
public:
	FilterException() noexcept(false) : FilteredSet(f), std::exception::exception("Filter exception") {}
	FilterException(const FilterException&);
	void print(std::ostream&) const noexcept override final;
};

template<class T>
FilterException<T>::FilterException(const FilterException& other) : FilterException()
{
	for (auto a : other)
		add({ a });
}

template<class T>
void FilterException<T>::print(std::ostream& os) const noexcept
{
	os << "Elements not placed:" << std::endl;
	for (auto it = begin(); it != end(); ++it)
		os << *it << ' ';
}