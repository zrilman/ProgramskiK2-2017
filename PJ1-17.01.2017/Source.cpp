#include "Kolokvijum.h"
#include <fstream>


constexpr long long faktoriel(const int& n = 20)
{
	return n* (n > 1 ? faktoriel(n - 1) : 1);
}

void main()
{
	constexpr long long a = 0b1111'0101'0010'0001;
	constexpr long long X = a ^ faktoriel();
	const std::function<bool(const int&)> f = [](const int& a) { return a > 0; };
	FilteredSet<int, X> filterSet(f);
	try
	{
		std::initializer_list<int> list1 = { 2,4,-2,5,2,-1 };
		std::initializer_list<int> list2 = { 1,2,3,-1 };
		filterSet.add(list1);
		filterSet.add(list2);
	}
	catch (FilterException<int> ex)
	{
		std::ofstream ofs("error_output.txt");
		if (ofs.is_open())
			ofs << ex;
		ofs.close();
	}

	filterSet.print(std::cout);
	std::cin.ignore();
	std::cin.get();
}