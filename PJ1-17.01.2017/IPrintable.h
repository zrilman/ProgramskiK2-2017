#pragma once
#include <iostream>

class IPrintable
{
public:
	virtual void print(std::ostream&) const noexcept = 0;
private:
	friend std::ostream& operator<< (std::ostream& os, const IPrintable& ip)
	{
		ip.print(os);
		return os;
	}
};